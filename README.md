# Anul 3
##Profesori curs:
* Retele: Iulian NEMEDI
* Tehnologii Web: Andrei TOMA
* Multimedia: Alexandru BARBULESCU
* Analiza Datelor: Felix FURTUNA
* Econometrie: Denisa VASILESCU
* Dispozitive si Aplicatii Mobile: Paul POCATILU

##Profesori seminar:
* Retele: Iulian NEMEDI
* Tehnologii Web: Cosmin CARTAS
* Multimedia: Alexandru BARBULESCU
* Analiza Datelor: Claudiu VINTE
* Econometrie: Simona APOSTU
* Dispozitive si Aplicatii Mobile: Alexandru DITA