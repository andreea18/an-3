#Tehnologii Web
## In 2019 s-a schimbat examenul si s-a dat pe calculator. In folderele cu "Examen 2019" sunt:
* "- subiecte cu cateva rezolvari": unde sunt chestiile puse de profi la ei pe bitbucket inainte de examen
* "- toate subiectele": unde sunt efectiv variantele care s-au dat la examen.

Chiar daca nu dau la fel in anii urmatori, sigur ajuta pentru exercitiu.

## Linkuri utile:
andrei.ase.ro
https://bitbucket.org/hypothetical_andrei/
https://bitbucket.org/ccartas/
https://www.webtech-superheroes.net/